# Pandoc markdown->pdf boilerplate
# ------------------------------------------------------------------------------

# Pandoc configuration ---------------------------------------------------------
# Pandoc command
PANDOC_BIN=pandoc

# Pandoc global settings
PANDOC_GLOBAL=\
	--from markdown

# Template file
PANDOC_TEMPLATE=

# PDF/LaTeX specific settings
PANDOC_PDF=\
	--latex-engine=xelatex\


# Source files/directories -----------------------------------------------------
# Documents
SRC_DOCDIR=docs

# Images
SRC_IMGDIR=img

# Metadata file
SRC_META=meta.yaml


# Outputs ----------------------------------------------------------------------
# Output directory
OUT_DIR=outputs

# Output name (without extensions)
OUT_NAME=doc

################################################################################

# Source paths generation
_SRC_DOCS:=$(realpath $(shell ls $(SRC_DOCDIR)/*.md))
_SRC_IMGS:=$(realpath $(shell ls $(SRC_IMGDIR)/*))
_SRC_META:=$(realpath $(SRC_META))
_SRC_ALLD:=$(_SRC_META) $(_SRC_DOCS) $(_SRC_IMGS)

# Pandoc template path
ifdef PANDOC_TEMPLATE
	_SRC_TEMP:=--template=$(realpath $(PANDOC_TEMPLATE))
else
	_SRC_TEMP=
endif

# Output path generation
_OUT_PATH:=$(realpath $(OUT_DIR))/$(OUT_NAME)

# Git
GIT_HASH:=--variable git-hash=$(shell git rev-parse --short HEAD)

# Generic target ---------------------------------------------------------------
.PHONY: all
all: pdf

# PDF Target -------------------------------------------------------------------
.PHONY: pdf
pdf: $(_OUT_PATH).pdf
$(_OUT_PATH).pdf: $(_SRC_ALLD)
	cd $(SRC_DOCDIR) && $(PANDOC_BIN) $(PANDOC_GLOBAL) $(PANDOC_PDF) $(GIT_HASH) $(_SRC_TEMP) $(_SRC_META) $(_SRC_DOCS) -o $(_OUT_PATH).pdf

# Clean ------------------------------------------------------------------------
.PHONY: clean
clean:
	rm $(_OUT_PATH).*
